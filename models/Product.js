const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid')
var os = require('os')

AWS.config.region = 'ap-southeast-2'

var PRODUCTS_TABLE = 'lvlup-demo.products'

if (os.hostname().indexOf('.local') != -1) {
  var credentials = new AWS.SharedIniFileCredentials({
    profile: 'claudia-se-demo'
  })
  AWS.config.credentials = credentials
  PRODUCTS_TABLE = 'lvlup-demo.products.local'
}

const dynamoDb = new AWS.DynamoDB.DocumentClient()

async function getProducts (id) {
  if (id) {
    return dynamoDb
      .get({ 
        TableName: PRODUCTS_TABLE,
        Key:{
          id: id
        }
      })
      .promise()
      .then(response => response.Item)
  } else {
    return await dynamoDb
      .scan({ TableName: PRODUCTS_TABLE })
      .promise()
      .then(response => response.Items)
  }
}

async function createOrUpdateProduct (data) {
  let id = uuidv4();

  if(data && data.id) {
    id = data.id;
  }

  console.log(id);

  let name = '';
  let description = ''
  let model = ''
  let sku = ''
  let cost = ''
  let imageUrl = ''

  if (data) {
    name = data.name ? data.name : ''
    description = data.description ? data.description : ''
    model = data.model ? data.model : ''
    sku = data.sku ? data.sku : ''
    cost = data.cost ? data.cost : ''
    imageUrl = data.imageUrl ? data.imageUrl : ''
  }

  if (
    name === '' ||
    description === '' ||
    model === '' ||
    sku === '' ||
    cost === '' ||
    imageUrl === ''
  ) {
    valid = false
    let result = {
      status: 'Failed',
      message:
        'The following fields are required to create a product: description, model, sku, cost, imageUrl.'
    }
    return result
  }

  var item = {
    id: id,
    name: name,
    description: description,
    model: model,
    sku: sku,
    cost: cost,
    imageUrl: imageUrl
  }

  var params = {
    TableName: PRODUCTS_TABLE,
    Item: item
  }
  return await dynamoDb
    .put(params)
    .promise()
    .then(() => {
      return item
    })
}

async function deleteProduct(id) {
  
  if (id) {
    return await dynamoDb
      .delete({ 
        TableName: PRODUCTS_TABLE,
        Key:{
          id: id
        }
      })
      .promise()
      .then((response) => {
        return {"status": 'OK'}
      });
  }

}

module.exports = {
  getProducts: getProducts,
  createOrUpdateProduct: createOrUpdateProduct,
  deleteProduct:deleteProduct
}

const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid')
var os = require('os')

AWS.config.region = 'ap-southeast-2'

var ORDERS_TABLE = 'lvlup-demo.orders'

if (os.hostname().indexOf('.local') != -1) {
  var credentials = new AWS.SharedIniFileCredentials({
    profile: 'claudia-se-demo'
  })
  AWS.config.credentials = credentials
  ORDERS_TABLE = 'lvlup-demo.orders.local'
}

const dynamoDb = new AWS.DynamoDB.DocumentClient()

async function getOrders (id) {
  if (id) {
    return dynamoDb
      .get({ 
        TableName: ORDERS_TABLE,
        Key:{
          id: id
        }
      })
      .promise()
      .then(response => response.Item)
  } else {
    return await dynamoDb
      .scan({ TableName: ORDERS_TABLE })
      .promise()
      .then(response => response.Items)
  }
}

async function createOrUpdateOrder (data) {
  let id = uuidv4();

  if(data && data.id) {
    id = data.id;
  }

  console.log(id);

  let status = '';
  let date = ''
  let productIds = []
  let cost = ''
  let tax = ''
  let taxRate = ''
  let total = ''

  if (data) {
    status = data.status ? data.status : ''
    date = data.date ? data.date : ''
    productIds = data.productIds ? data.productIds : ''
    cost = data.cost ? data.cost : ''
    tax = data.tax ? data.tax : ''
    taxRate = data.taxRate ? data.taxRate : ''
    total = data.total ? data.total : ''
  }

  if (
    status === '' ||
    date === '' ||
    productIds === '' ||
    cost === '' ||
    tax === '' ||
    taxRate === '' ||
    total === ''
  ) {
    valid = false
    let result = {
      status: 'Failed',
      message:
        'The following fields are required to create an order: status, date, productIds, cost, tax, taxRate, total.'
    }
    return result
  }

  var item = {
    id: id,
    status: status,
    date: date,
    productIds: productIds,
    cost: cost,
    tax: tax,
    taxRate: taxRate,
    total: total
  }

  var params = {
    TableName: ORDERS_TABLE,
    Item: item
  }
  return await dynamoDb
    .put(params)
    .promise()
    .then(() => {
      return item
    })
}

async function deleteOrder(id) {
  
  if (id) {
    return await dynamoDb
      .delete({ 
        TableName: ORDERS_TABLE,
        Key:{
          id: id
        }
      })
      .promise()
      .then((response) => {
        return {"status": 'OK'}
      });
  }

}

module.exports = {
  getOrders: getOrders,
  createOrUpdateOrder: createOrUpdateOrder,
  deleteOrder:deleteOrder
}

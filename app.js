//index.js
const express = require('express')
const exphbs = require('express-handlebars')
const qs = require('qs')

const Product = require('./models/Product.js')
const Order = require('./models/Order.js')

let app = express()
let hbs = exphbs.create({
  defaultLayout: 'main',
  extname: '.hbs',
  layoutsDir: `${__dirname}/views/layouts`,
  partialsDir: [__dirname + '/views/partials/']
})

//Set the static files directory
app.use(express.static(`${__dirname}/views/assets`))

//Set the view engine
app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')

app.use(
  express.urlencoded({
    extended: true
  })
)

app.use(express.json())

app.get('/', (req, res) => {
  let prefix = '/latest'

  //Check if we need a prefix
  if (req.headers.host.indexOf('localhost') != -1) {
    prefix = ''
  }

  res.render('index', {
    prefix: prefix
  })
})


//Products Endpoints
app.get('/api/products', (req, res) => {
  return Product.getProducts().then(products => {
    return res.status(200).json(products)
  })
})

app.post('/api/products', (req, res) => {
  let data = qs.parse(req.body)

  Product.createOrUpdateProduct(data)
    .then(result => {
      console.log(result)
      if (result.status == 'OK') {
        return res.status(201).json(result)
      } else {
        return res.status(422).json(result)
      }
    })
    .catch(err => {
      return res.status(422).json(err)
    })
})


//Products By ID Endpoints
app.get('/api/products/:productId', (req, res) => {
  var productId = req.params.productId

  if (!productId) {
    return res.status(400).json({
      status: 422,
      message: 'productId is a required path parameter'
    })
  } else {
    return Product.getProducts(productId).then(products => {
      if(!products) {
        return res.status(404).json({
          status: 404,
          message: "Product ID not found"
        });
      } else {
        return res.status(200).json(products)
      }
      
    })
  }
})

app.put('/api/products/:productId', (req, res) => {
  var productId = req.params.productId

  if (!productId) {
    return res.status(400).json({
      status: 422,
      message: 'productId is a required path parameter'
    })
  } else {
    let data = qs.parse(req.body);
    data.id = productId;
    Product.createOrUpdateProduct(data)
      .then(result => {
        console.log(result)
        if (result.status == 'OK') {
          return res.status(200).json(result)
        } else {
          return res.status(422).json(result)
        }
      })
      .catch(err => {
        return res.status(422).json(err)
      })
  }
})

app.delete('/api/products/:productId', (req, res) => {
  var productId = req.params.productId

  if (!productId) {
    return res.status(400).json({
      status: 422,
      message: 'productId is a required path parameter'
    })
  } else {
    Product.deleteProduct(productId)
      .then(result => {
        console.log(result)
        if (result.status == 'OK') {
          return res.status(204).json()
        } else {
          return res.status(422).json(result)
        }
      })
      .catch(err => {
        return res.status(422).json(err)
      })
  }
})


//Orders Endpoints
app.get('/api/orders', (req, res) => {
  return Order.getOrders().then(orders => {
    return res.status(200).json(orders)
  })
})

app.post('/api/orders', (req, res) => {
  let data = qs.parse(req.body)

  Order.createOrUpdateOrder(data)
    .then(result => {
      console.log(result)
      if (result.status == 'OK') {
        return res.status(201).json(result)
      } else {
        return res.status(422).json(result)
      }
    })
    .catch(err => {
      return res.status(422).json(err)
    })
})


//Orders By ID Endpoints
app.get('/api/orders/:orderId', (req, res) => {
  var orderId = req.params.orderId

  if (!orderId) {
    return res.status(400).json({
      status: 422,
      message: 'orderId is a required path parameter'
    })
  } else {
    return Order.getOrders(orderId).then(orders => {
      if(!orders) {
        return res.status(404).json({
          status: 404,
          message: "Order ID not found"
        });
      } else {
        return res.status(200).json(orders)
      }
      
    })
  }
})

app.put('/api/orders/:orderId', (req, res) => {
  var orderId = req.params.orderId

  if (!orderId) {
    return res.status(400).json({
      status: 422,
      message: 'orderId is a required path parameter'
    })
  } else {
    let data = qs.parse(req.body);
    data.id = orderId;
    Order.createOrUpdateOrder(data)
      .then(result => {
        console.log(result)
        if (result.status == 'OK') {
          return res.status(200).json(result)
        } else {
          return res.status(422).json(result)
        }
      })
      .catch(err => {
        return res.status(422).json(err)
      })
  }
})

app.delete('/api/orders/:orderId', (req, res) => {
  var orderId = req.params.orderId

  if (!orderId) {
    return res.status(400).json({
      status: 422,
      message: 'orderId is a required path parameter'
    })
  } else {
    Order.deleteOrder(orderId)
      .then(result => {
        console.log(result)
        if (result.status == 'OK') {
          return res.status(204).json()
        } else {
          return res.status(422).json(result)
        }
      })
      .catch(err => {
        return res.status(422).json(err)
      })
  }
})



app.listen(3000, () => {
  console.log('Products & Orders API listening on 3000!')
})

module.exports = app
